package com.ss.godt.Service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import com.ss.godt.HTTP.HttpClient;
import com.ss.godt.HTTP.HttpRequestAPI;
import com.ss.godt.HTTP.Model.ImageModel;
import com.ss.godt.HTTP.Model.IngredientsModel;
import com.ss.godt.HTTP.Model.RecipeModel;
import com.ss.godt.Provider.DataProvider;

import java.util.List;

/**
 * Created by Sokol on 8/11/2015.
 */
public class SynchronizationService extends Service {

    public final static String THREAD_NAME = "threadService";

    private final IBinder binder = new ServiceBinder();
    private LooperHandler mLooperHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread handlerThread = new HandlerThread(THREAD_NAME, android.os.Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        mLooperHandler = new LooperHandler(handlerThread.getLooper());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class ServiceBinder extends Binder {
        public SynchronizationService getService() {
            return SynchronizationService.this;
        }
    }

    public void downloadFromServer() {
        Message message = mLooperHandler.obtainMessage();
        message.obj = "";
        mLooperHandler.sendMessage(message);
    }

    private class LooperHandler extends Handler {

        public LooperHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (!isNetworkConnected()) {
                return;
            }

            HttpRequestAPI api = HttpClient.getInstance();
            List<RecipeModel> content = api.requestsRecipesList("", "thumbnail-medium", 1, 50, 0);


            for (RecipeModel model : content) {
                ContentValues contentValues = RecipeModel.getContentValues(model);
                if (getContentResolver().insert(DataProvider.CONTENT_URI_RECIPES, contentValues) == null) {
                    break;
                }
                for (IngredientsModel ingredientsModel : model.ingredients) {
                    ContentValues contentValues1 = IngredientsModel.getContentValues(model.id, ingredientsModel);
                    getContentResolver().insert(DataProvider.CONTENT_URI_ELEMENTS, contentValues1);
                }
                for (ImageModel imageModel : model.images) {
                    ContentValues contentValues1 = ImageModel.getContentValues(model.id, imageModel);
                    getContentResolver().insert(DataProvider.CONTENT_URI_IMAGES, contentValues1);
                }
            }


        }

        private boolean isNetworkConnected() {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() == null) {
                return false;
            } else
                return true;
        }
    }


}
