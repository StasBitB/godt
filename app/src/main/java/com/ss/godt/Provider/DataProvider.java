package com.ss.godt.Provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.ss.godt.DB.DatabaseHelper;

/**
 * Created by Sokol on 8/11/2015.
 */
public class DataProvider extends ContentProvider {

    public static final String AUTHORITY = "com.ss.godt.DB";

    public static Uri CONTENT_URI_RECIPES = Uri.parse("content://" + DataProvider.AUTHORITY + "/" + DatabaseHelper.TABLE_RECIPE);
    public static Uri CONTENT_URI_ELEMENTS = Uri.parse("content://" + DataProvider.AUTHORITY + "/" + DatabaseHelper.TABLE_INGREDIENTS);
    public static Uri CONTENT_URI_IMAGES = Uri.parse("content://" + DataProvider.AUTHORITY + "/" + DatabaseHelper.TABLE_IMAGES);

    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mSqLiteDatabase;
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI("com.ss.godt.DB", DatabaseHelper.TABLE_RECIPE, 1);
        sURIMatcher.addURI("com.ss.godt.DB", DatabaseHelper.TABLE_INGREDIENTS, 2);
        sURIMatcher.addURI("com.ss.godt.DB", DatabaseHelper.TABLE_IMAGES, 3);
    }

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        Cursor cursor = null;
        switch (sURIMatcher.match(uri)) {
            case 1:

                String MY_QUERY = "SELECT * FROM recipe JOIN ingredients ON ingredients.elements_id = recipe.id JOIN images ON images.elements_id = recipe.id ";

                if (selection != null) MY_QUERY += "where " + selection;

                cursor = mSqLiteDatabase.rawQuery(MY_QUERY, selectionArgs);
                break;
            case 2:
                cursor = mSqLiteDatabase.query(DatabaseHelper.TABLE_INGREDIENTS, null, selection, selectionArgs, null, null, null);
                break;
            case 3:
                cursor = mSqLiteDatabase.query(DatabaseHelper.TABLE_IMAGES, null, selection, selectionArgs, null, null, null);
                break;
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        long insertId = 0;
        switch (sURIMatcher.match(uri)) {
            case 1:
                insertId = mSqLiteDatabase.insert(DatabaseHelper.TABLE_RECIPE, null, values);
                break;
            case 2:
                insertId = mSqLiteDatabase.insert(DatabaseHelper.TABLE_INGREDIENTS, null, values);
                break;
            case 3:
                insertId = mSqLiteDatabase.insert(DatabaseHelper.TABLE_IMAGES, null, values);
                break;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        if (insertId == -1)
            return null;
        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
