package com.ss.godt.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sokol on 8/11/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "godt";

    public static final String TABLE_RECIPE = "recipe";
    public static final String TABLE_INGREDIENTS = "ingredients";
    public static final String TABLE_IMAGES = "images";

    public static final String RECIPE_KEY_ID = "id";
    public static final String RECIPE_KEY_TITLE = "title";
    public static final String RECIPE_KEY_DESCRIPTION = "description";
    public static final String RECIPE_KEY_BASIC_PORTION_NUMBER = "basicPortionNumber";
    public static final String RECIPE_KEY_PREPARATION_TIME = "preparationTime";
    public static final String RECIPE_KEY_NUMBER_OF_COMMENTS = "numberOfComments";
    public static final String RECIPE_KEY_NUMBER_OF_LIKES = "numberOfLikes";
    public static final String RECIPE_KEY_PUBLISHED_AT = "publishedAt";
    public static final String RECIPE_KEY_STATUS = "status";
    public static final String RECIPE_KEY_UPDATE_AT = "updatedAt";
    public static final String RECIPE_KEY_EMBED_TITLE = "embedTitle";

    public static final String INGREDIENTS_KEY_ID = "ingredientsId";
    public static final String INGREDIENTS_KEY_RECIPE_ID = "elements_id";
    public static final String INGREDIENTS_KEY_NAME = "name";

    public static final String IMAGES_KEY_IMBOID = "imboId";
    public static final String IMAGES_KEY_RECIPE_ID = "elements_id";
    public static final String IMAGES_KEY_URL = "url";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_RECIPE + "("
                + RECIPE_KEY_ID + " INTEGER PRIMARY KEY NOT NULL," + RECIPE_KEY_TITLE + " TEXT," +
                RECIPE_KEY_DESCRIPTION + " TEXT," + RECIPE_KEY_BASIC_PORTION_NUMBER + " INTEGER," +
                RECIPE_KEY_PREPARATION_TIME + " INTEGER," + RECIPE_KEY_NUMBER_OF_COMMENTS + " INTEGER," +
                RECIPE_KEY_NUMBER_OF_LIKES + " INTEGER," + RECIPE_KEY_PUBLISHED_AT + " TEXT," +
                RECIPE_KEY_STATUS + " TEXT," + RECIPE_KEY_UPDATE_AT + " TEXT," +
                RECIPE_KEY_EMBED_TITLE + " TEXT)");
        db.execSQL("CREATE TABLE " + TABLE_INGREDIENTS + "(" + INGREDIENTS_KEY_ID + " TEXT,"
                + INGREDIENTS_KEY_RECIPE_ID + " INTEGER REFERENCES recipe(_id)," + INGREDIENTS_KEY_NAME + " TEXT)");
        db.execSQL("CREATE TABLE " + TABLE_IMAGES + "(" + IMAGES_KEY_IMBOID + " TEXT,"
                + IMAGES_KEY_RECIPE_ID + " INTEGER REFERENCES recipe(_id)," + IMAGES_KEY_URL + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
