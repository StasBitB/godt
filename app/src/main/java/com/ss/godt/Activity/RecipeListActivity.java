package com.ss.godt.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ss.godt.R;

public class RecipeListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
    }
}
