package com.ss.godt.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ss.godt.R;

/**
 * Created by Sokol on 8/12/2015.
 */
public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }

    public static void initActionBar(String title, AppCompatActivity activity) {
        AppCompatActivity actionBarActivity = activity;
        actionBarActivity.getSupportActionBar().setTitle(title);
        actionBarActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        actionBarActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
