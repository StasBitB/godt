package com.ss.godt.Fragment;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;

import com.ss.godt.Activity.SearchActivity;
import com.ss.godt.Adapter.AdapterModel.Recipe;
import com.ss.godt.Adapter.RecipeAdapter;
import com.ss.godt.DB.DatabaseHelper;
import com.ss.godt.Provider.DataProvider;
import com.ss.godt.R;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Sokol on 8/12/2015.
 */
public class SearchFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private RadioGroup mRadioGroup;
    private EditText mEditText;
    private String mSelectedField;
    private RecipeAdapter mRecipeAdapter;
    private static final int LOADER_ID = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        SearchActivity.initActionBar(getResources().getString(R.string.action_bar_search), (AppCompatActivity) getActivity());
        mSelectedField = DatabaseHelper.RECIPE_KEY_TITLE;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search, container, false);

        mEditText = (EditText) root.findViewById(R.id.searchEditText);
        mRadioGroup = (RadioGroup) root.findViewById(R.id.radio);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioTitle:
                        mSelectedField = DatabaseHelper.RECIPE_KEY_TITLE;
                        reload(mEditText.getText().toString());
                        break;
                    case R.id.radioIngredients:
                        mSelectedField = DatabaseHelper.INGREDIENTS_KEY_NAME;
                        reload(mEditText.getText().toString());
                        break;

                }

            }
        });
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                reload(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ListView listView = (ListView) root.findViewById(R.id.listView);
        mRecipeAdapter = new RecipeAdapter(new HashMap<Integer, Recipe>(), getActivity());
        listView.setAdapter(mRecipeAdapter);

        return root;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String text = mEditText.getText().toString();
        CursorLoader cursorLoader = null;
        switch (mSelectedField) {
            case DatabaseHelper.RECIPE_KEY_TITLE:
                cursorLoader = new CursorLoader(getActivity(),
                        DataProvider.CONTENT_URI_RECIPES, null, DatabaseHelper.RECIPE_KEY_TITLE + " LIKE ?", new String[]{text + "%"}, null);
                break;
            case DatabaseHelper.INGREDIENTS_KEY_NAME:

//                For few
//                String s = " _id IN (SELECT elements_id FROM ingredients WHERE coalesce(name, '') LIKE ? ) and _id IN (SELECT elements_id FROM ingredients WHERE coalesce(name, '') LIKE ? )" ;
//
//                cursorLoader = new CursorLoader(getActivity(),
//                        DataProvider.CONTENT_URI_RECIPES, null, s, new String[]{"Sorbet%","Sjoko%"}, null);
//                break;

                cursorLoader = new CursorLoader(getActivity(),
                        DataProvider.CONTENT_URI_RECIPES, null, "coalesce(" + DatabaseHelper.INGREDIENTS_KEY_NAME + ", '') LIKE ?", new String[]{text + "%"}, null);
                break;

        }
        return cursorLoader;
    }

    private void reload(String s) {
        if (s.isEmpty()) {
            mRecipeAdapter.clean();
        } else
            getLoaderManager().restartLoader(LOADER_ID, null, SearchFragment.this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Map<Integer, Recipe> map = new TreeMap<>();

        while (data.moveToNext()) {
            Integer id = data.getInt(data.getColumnIndexOrThrow(DatabaseHelper.RECIPE_KEY_ID));

            if (map.containsKey(id)) {
                Recipe recipe = map.get(id);
                recipe.addIngredient(data.getString(data.getColumnIndexOrThrow(DatabaseHelper.INGREDIENTS_KEY_NAME)));
            } else {
                String description = data.getString(data.getColumnIndexOrThrow(DatabaseHelper.RECIPE_KEY_DESCRIPTION));
                String title = data.getString(data.getColumnIndexOrThrow(DatabaseHelper.RECIPE_KEY_TITLE));
                Recipe recipe = new Recipe(title, description, id);
                recipe.addIngredient(data.getString(data.getColumnIndexOrThrow(DatabaseHelper.INGREDIENTS_KEY_NAME)));
                recipe.addImage(data.getString(data.getColumnIndexOrThrow(DatabaseHelper.IMAGES_KEY_URL)));
                map.put(id, recipe);
            }
        }

        mRecipeAdapter.setMap(map);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
