package com.ss.godt.Fragment;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ss.godt.Activity.SearchActivity;
import com.ss.godt.Adapter.AdapterModel.Recipe;
import com.ss.godt.Adapter.RecipeAdapter;
import com.ss.godt.DB.DatabaseHelper;
import com.ss.godt.Provider.DataProvider;
import com.ss.godt.R;
import com.ss.godt.Service.SynchronizationService;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Sokol on 8/11/2015.
 */
public class RecipeListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private RecipeAdapter mRecipeListAdapter;
    private SynchronizationService mService;
    private static final int LOADER_ID = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recipe_list, container, false);

        ListView listView = (ListView) root.findViewById(R.id.listView);
        mRecipeListAdapter = new RecipeAdapter(new HashMap<Integer, Recipe>(), getActivity());
        listView.setAdapter(mRecipeListAdapter);

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return root;
    }


    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(getActivity().getBaseContext(), SynchronizationService.class);
        getActivity().startService(intent);
        getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unbindService(serviceConnection);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Intent intent = new Intent();
                intent.setClass(getActivity(), SearchActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader cursorLoader = new CursorLoader(getActivity(),
                DataProvider.CONTENT_URI_RECIPES, null, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        Map<Integer, Recipe> map = new LinkedHashMap<>();

        while (data.moveToNext()) {
            Integer id = data.getInt(data.getColumnIndexOrThrow(DatabaseHelper.RECIPE_KEY_ID));

            if (map.containsKey(id)) {
                Recipe recipe = map.get(id);
                recipe.addIngredient(data.getString(data.getColumnIndexOrThrow(DatabaseHelper.INGREDIENTS_KEY_NAME)));
            } else {
                String description = data.getString(data.getColumnIndexOrThrow(DatabaseHelper.RECIPE_KEY_DESCRIPTION));
                String title = data.getString(data.getColumnIndexOrThrow(DatabaseHelper.RECIPE_KEY_TITLE));
                Recipe recipe = new Recipe(title, description, id);
                recipe.addIngredient(data.getString(data.getColumnIndexOrThrow(DatabaseHelper.INGREDIENTS_KEY_NAME)));
                recipe.addImage(data.getString(data.getColumnIndexOrThrow(DatabaseHelper.IMAGES_KEY_URL)));
                map.put(id, recipe);
            }
        }

        mRecipeListAdapter.setMap(map);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SynchronizationService.ServiceBinder serviceBinder = (SynchronizationService.ServiceBinder) service;
            mService = serviceBinder.getService();
            mService.downloadFromServer();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

}
