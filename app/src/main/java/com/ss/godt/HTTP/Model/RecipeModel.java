package com.ss.godt.HTTP.Model;

import android.content.ContentValues;
import android.text.Html;

import com.ss.godt.DB.DatabaseHelper;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sokol on 8/11/2015.
 */
public class RecipeModel implements Serializable {

    public String title;
    public String description;
    public int basicPortionNumber;
    public int preparationTime;
    public int numberOfComments;
    public int numberOfLikes;
    public String publishedAt;
    public String status;
    public String updatedAt;
    public String embedTitle;
    public int id;
    public List<ImageModel> images;
    public List<IngredientsModel> ingredients;


    public static ContentValues getContentValues(RecipeModel model) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseHelper.RECIPE_KEY_ID, model.id);
        contentValues.put(DatabaseHelper.RECIPE_KEY_TITLE, model.title);
        contentValues.put(DatabaseHelper.RECIPE_KEY_DESCRIPTION, Html.fromHtml(model.description.replaceAll("<br />", "")).toString());
        contentValues.put(DatabaseHelper.RECIPE_KEY_BASIC_PORTION_NUMBER, model.basicPortionNumber);
        contentValues.put(DatabaseHelper.RECIPE_KEY_PREPARATION_TIME, model.preparationTime);
        contentValues.put(DatabaseHelper.RECIPE_KEY_NUMBER_OF_COMMENTS, model.numberOfComments);
        contentValues.put(DatabaseHelper.RECIPE_KEY_NUMBER_OF_LIKES, model.numberOfLikes);
        contentValues.put(DatabaseHelper.RECIPE_KEY_PUBLISHED_AT, model.publishedAt);
        contentValues.put(DatabaseHelper.RECIPE_KEY_STATUS, model.status);
        contentValues.put(DatabaseHelper.RECIPE_KEY_UPDATE_AT, model.updatedAt);
        contentValues.put(DatabaseHelper.RECIPE_KEY_EMBED_TITLE, model.embedTitle);

        return contentValues;
    }
}
