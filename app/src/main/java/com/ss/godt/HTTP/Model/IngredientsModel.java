package com.ss.godt.HTTP.Model;

import android.content.ContentValues;

import com.ss.godt.DB.DatabaseHelper;

/**
 * Created by Sokol on 8/11/2015.
 */
public class IngredientsModel {

    public String id;
    public String name;

    public static ContentValues getContentValues(int id, IngredientsModel model) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseHelper.INGREDIENTS_KEY_ID, model.id);
        contentValues.put(DatabaseHelper.INGREDIENTS_KEY_NAME, model.name);
        contentValues.put(DatabaseHelper.INGREDIENTS_KEY_RECIPE_ID, id);

        return contentValues;
    }
}
