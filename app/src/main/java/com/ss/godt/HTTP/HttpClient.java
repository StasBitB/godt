package com.ss.godt.HTTP;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Sokol on 8/11/2015.
 */
public class HttpClient {

    private final static String SERVER_URL_API = "http://www.godt.no/api";

    private static HttpRequestAPI instance;

    public static synchronized HttpRequestAPI getInstance() {
        if (instance == null) {

            Gson gson = new GsonBuilder()
                    .create();

            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint(SERVER_URL_API)
                    .setConverter(new GsonConverter(gson))
                    .build();
            instance = adapter.create(HttpRequestAPI.class);
        }
        return instance;
    }

}
