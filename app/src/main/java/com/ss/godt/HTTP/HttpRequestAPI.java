package com.ss.godt.HTTP;

import com.ss.godt.HTTP.Model.RecipeModel;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Sokol on 8/11/2015.
 */
public interface HttpRequestAPI {

    @GET("/getRecipesListDetailed")
    List<RecipeModel> requestsRecipesList(@Query("tags") String tags, @Query("size") String size, @Query("ratio") int ratio, @Query("limit") int limit, @Query("from") int from);

}
