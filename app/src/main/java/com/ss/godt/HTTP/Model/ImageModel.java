package com.ss.godt.HTTP.Model;

import android.content.ContentValues;

import com.ss.godt.DB.DatabaseHelper;

/**
 * Created by Sokol on 8/11/2015.
 */
public class ImageModel {
    public String imboId;
    public String url;

    public static ContentValues getContentValues(int id, ImageModel model) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseHelper.IMAGES_KEY_IMBOID, model.imboId);
        contentValues.put(DatabaseHelper.IMAGES_KEY_RECIPE_ID, id);
        contentValues.put(DatabaseHelper.IMAGES_KEY_URL, model.url);

        return contentValues;
    }
}
