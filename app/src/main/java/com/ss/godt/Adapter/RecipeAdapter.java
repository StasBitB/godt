package com.ss.godt.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.godt.Adapter.AdapterModel.Recipe;
import com.ss.godt.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Sokol on 8/13/2015.
 */
public class RecipeAdapter extends BaseAdapter {

    private List<Recipe> mList;
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public RecipeAdapter(Map<Integer, Recipe> map, Context context) {
        mList = new ArrayList<>(map.values());
        mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setMap(Map<Integer, Recipe> map) {
        mList = new ArrayList<>(map.values());
        notifyDataSetChanged();
    }

    public void clean() {
        mList = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View root = mLayoutInflater.inflate(R.layout.list_item, parent, false);

        TextView titleView = (TextView) root.findViewById(R.id.recipeTitle);
        TextView descriptionView = (TextView) root.findViewById(R.id.recipeDescription);
        ImageView imageView = (ImageView) root.findViewById(R.id.recipeImage);
        LinearLayout ingredientsView = (LinearLayout) root.findViewById(R.id.recipeIngredients);

        Recipe recipe = mList.get(position);

        titleView.setText(recipe.title);
        descriptionView.setText(recipe.description);

        for (int i = 0; i < recipe.ingredients.size(); i++) {
            TextView tv = (TextView) mLayoutInflater.inflate(R.layout.text_view, null);
            tv.setText(recipe.ingredients.get(i));
            ingredientsView.addView(tv);
        }

        Picasso.with(mContext).load(recipe.images.get(0)).resize(200, 200).centerCrop().into(imageView);

        return root;
    }

}
