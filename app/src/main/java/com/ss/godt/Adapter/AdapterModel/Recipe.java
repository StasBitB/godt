package com.ss.godt.Adapter.AdapterModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sokol on 8/13/2015.
 */
public class Recipe {

    public String title;
    public String description;
    public int id;
    public List<String> ingredients;
    public List<String> images;


    public Recipe(String title, String description, int id) {
        this.title = title;
        this.description = description;
        this.id = id;
        this.ingredients = new ArrayList<>();
        this.images = new ArrayList<>();
    }

    public void addIngredient(String string) {
        ingredients.add(string);
    }

    public void addImage(String string) {
        images.add(string);
    }
}
